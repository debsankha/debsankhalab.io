<!--
.. title: Creating this website
.. slug: creating-this-website
.. date: 2019-09-17 21:32:37 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I wanted a personal homepage/blog. I very often jot down my thoughts using 
[Jupyter](https://jupyter.org/) notebooks or MarkDown documents, so I looked 
for a tool that can create an website out of documents in these two formats. 
Since I *love* [CI/CD pipelines](https://docs.gitlab.com/ee/ci/pipelines.html),
and totally wanted my future site to be automatically deployed to [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)
on each commit, I decided to use a static site generator (SSG). There are quite a few
good SSG's, e.g. [Pelican](https://docs.getpelican.com/en/stable/),
[Jekyll](https://jekyllrb.com/), [Hugo](https://gohugo.io/), to name a few.

I settled on [Nikola](https://www.getnikola.com/handbook.html#post-list) because
I liked its documentations, out of the box support for Jupyter nokebooks and
ease of configuring via a python file.


I roughly followed the guide by [Jaakko Luttinen](http://www.jaakkoluttinen.fi/blog/how-to-blog-with-jupyter-ipython-notebook-and-nikola/).
The steps I took were:

- Install nikola in a virtualenv.
- Run `nikola init` to initialize a website.
- Made jupyter notebooks the default format for posts in `conf.py`.
- Enable MathJax rendering of $...$ delimited text.
- Choose a theme by playing around with `nikola subtheme`.
- Add CC-BY-SA license.
- Configure `GitLab CI` so that on each commit, a fresh copy of the site gets deployed to GitLab pages.

