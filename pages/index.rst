.. title: 🏠 About me
.. slug: index
.. date: 2019-09-23 20:09:47 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

I am Debsankha Manik - a theoretical physicist, python programmer and data scientist.
My research topics include:

- Improving human mobility systems with on-demand ridepooling.
- Understanding how supply networks, e.g. the electrical power grid, are
  affected by connectivity patterns.

Programming related topics I am interested in includes:

- Data analysis in python, using pandas and dask.
- Data visualization, using various `tools in the python ecosystem <https://pyviz.org/tools.html>`_.
- Data processing pipelines using `luigi <https://github.com/spotify/luigi>`_ and `dask <https://docs.dask.org/en/latest/>`_.
- `Simulating on-demand mobility <https://dl.acm.org/citation.cfm?id=2888940>`_ systems.
- `Continuous integration <https://gitlab.gwdg.de/dmanik/gitlab-ci-talk>`_ pipelines.
- `Teaching data science <https://www.uni-goettingen.de/de/daten+lesen+lernen/592287.html>`_.
- Infrastructure as code using `Ansible <https://www.ansible.com/>`_.
- Data analysis and visualization, especially geospatial and network data.
- Interactive data visualization dashboards.
- C-extensions for python.
- Parallel computing using `dask`_, `ipyparallel  <https://ipyparallel.readthedocs.io/en/latest/>`_ and grid engine.


**My public key fingerprint:** (please sign it)

0xC6FAAA17295D22965A9F8F3EA3423F4523ACAE71



**Recent blog posts:**

.. post-list::
   :stop: 5
